# Grupo 04 - Catraca inteligente integrada ao Decidim

| Integrantes                   |
|------------------------|
| Abner Filipe           |
| Allecsander Lélis      |
| Ana Aparecida          |
| Bianca Sofia     |
| Daniel Primo           |
| Júlio César            |
| Lorrany Souza          |
| Lorrayne Alves        |
| Lucas Pimentel         |
| Nicolas                |
| Marina Costa           |
| Pedro Campos           |
| Rafael Leão            |
| Ramires                |
| Thiago Galletti        |
| Thiago França Vale Oliveira |
| Vitor Magalhães        |


## [Apresentação do PC3](https://drive.google.com/file/d/1dfBzPp5R1y7ZLw2JiAbD70muKQszQIoX/view?usp=sharing)

## [Documentação do PC3](./ponto_de_controle_3.pdf)
